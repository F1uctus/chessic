package com.f1uctus.chessic.maps;

import android.graphics.Bitmap;

import com.crown.i18n.I18n;
import com.crown.i18n.ITemplate;
import com.crown.maps.MapIcon;

public class GraphicalMapIcon extends MapIcon<Bitmap> {
    private final Bitmap icon;

    public GraphicalMapIcon(String keyName, Bitmap icon) {
        super(keyName);
        this.icon = icon;
    }

    @Override
    public ITemplate getName() {
        return I18n.empty;
    }

    @Override
    public ITemplate getDescription() {
        return I18n.empty;
    }

    public Bitmap get() {
        return icon;
    }

    @Override
    public void stepAnimation() {
    }
}