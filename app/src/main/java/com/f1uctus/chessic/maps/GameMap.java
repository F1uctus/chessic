package com.f1uctus.chessic.maps;

import com.crown.i18n.I18n;
import com.crown.i18n.ITemplate;
import com.crown.maps.Map;

public class GameMap extends Map {
    public GameMap(int xSize, int ySize, int zSize) {
        super("Game map", xSize, ySize, zSize);
    }

    @Override
    public GraphicalMapIcon getEmptyIcon() {
        return null;
    }

    @Override
    public ITemplate getName() {
        return I18n.empty;
    }

    @Override
    public ITemplate getDescription() {
        return I18n.empty;
    }
}