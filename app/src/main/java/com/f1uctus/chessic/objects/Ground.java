package com.f1uctus.chessic.objects;

import com.crown.maps.Map;
import com.crown.maps.MapIcon;
import com.crown.maps.MapObject;
import com.crown.maps.Point3D;
import com.crown.time.Timeline;
import com.f1uctus.chessic.GameState;
import com.f1uctus.chessic.maps.GraphicalMapIcon;

public class Ground extends MapObject {
    public Ground(
        String name,
        Map map,
        GraphicalMapIcon icon,
        Point3D pt0
    ) {
        super(name, map, icon, pt0);
    }

    @Override
    public boolean isTransparent() {
        return true;
    }

    @Override
    public boolean isWalkable() {
        return true;
    }

    @Override
    public MapIcon<?> getMapIcon() {
        return ((GameState) Timeline.main.getGameState()).iconThemes
            .get("default")
            .get(getMapIconId());
    }
}
