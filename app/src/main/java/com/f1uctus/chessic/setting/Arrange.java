package com.f1uctus.chessic.setting;

public class Arrange {
    private String team;
    private String unit;
    private int x;
    private int y;
    private int z;

    Arrange() {
    }

    public Arrange(String team, String unit, int x, int y, int z) {
        this.team = team;
        this.unit = unit;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String getTeam() {
        return team;
    }

    public String getUnit() {
        return unit;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
}
