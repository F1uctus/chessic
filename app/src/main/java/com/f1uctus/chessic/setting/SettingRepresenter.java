package com.f1uctus.chessic.setting;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.introspector.Property;
import org.yaml.snakeyaml.representer.Representer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class SettingRepresenter extends Representer {
    public SettingRepresenter() {
    }

    public SettingRepresenter(DumperOptions options) {
        super(options);
    }

    protected Set<Property> getProperties(Class<?> type) {
        List<Property> propsList = new ArrayList<>(getPropertyUtils().getProperties(type));
        propsList.sort(new BeanPropertyComparator());
        return new LinkedHashSet<>(propsList);
    }

    static class BeanPropertyComparator implements Comparator<Property> {
        List<String> predefinedOrder = Arrays.asList(
            "name",
            "map",
            "xSize",
            "ySize",
            "zSize",
            "formula",
            "teams",
            "units",
            "arrangement",
            "allowedMovements"
        );

        public int compare(Property p1, Property p2) {
            String n1 = p1.getName();
            String n2 = p2.getName();
            if (predefinedOrder.contains(n1) && predefinedOrder.contains(n2)) {
                return predefinedOrder.indexOf(n1) - predefinedOrder.indexOf(n2);
            }
            return 1;
        }
    }
}