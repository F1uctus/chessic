package com.f1uctus.chessic.setting;

import com.crown.maps.Direction;
import com.crown.maps.Map;
import com.crown.maps.MapObject;
import com.crown.maps.Point3D;
import com.f1uctus.chessic.creatures.Unit;
import com.f1uctus.chessic.tools.MathTools;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

public class Movement {
    private int x;
    private int y;
    private int z;
    private Direction direction;
    private TargetType type;

    Movement() {
    }

    public Movement(int x, int y, TargetType targetType) {
        this.x = x;
        this.y = y;
        direction = Direction.none;
        type = targetType;
    }

    public Movement(Direction direction, TargetType type) {
        this.direction = direction;
        this.type = type;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int y) {
        this.z = z;
    }

    public TargetType getType() {
        return type;
    }

    public void setType(TargetType type) {
        this.type = type;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public boolean contains(Unit u, int x, int y) {
        return getPoints(u).stream().anyMatch(p -> p.x == x && p.y == y);
    }

    public boolean contains(Unit u, int x, int y, int z) {
        return getPoints(u).stream().anyMatch(p -> p.x == x && p.y == y && p.z == z);
    }

    public boolean isAllowed(MapObject a, MapObject b) {
        if (!(b instanceof Unit)) {
            return type != Movement.TargetType.enemy;
        } else {
            return type == Movement.TargetType.enemy
                || type == Movement.TargetType.any
                || type == Movement.TargetType.groundTillEnemy
                && direction == Direction.fromPoint(b.getPt0().minus(a.getPt0()));
        }
    }

    public List<Point3D> getPoints(Unit a) {
        Map m = a.getMap();
        List<Point3D> points = new ArrayList<>();
        Point3D d = new Point3D(
            direction != Direction.none
                ? direction.point.x
                : x,
            direction != Direction.none
                ? direction.point.y
                : y,
            // if no Z movement meant, Z delta is effectively 0
            z != 0 || direction.point.z != 0
                ? MathTools.maxIgnoreSign(z, direction.point.z)
                : 0
        );

        BiFunction<Unit, MapObject, Boolean> groundOrEnemy = (u, b) ->
            !(b instanceof Unit) || ((Unit) b).team != u.team;

        Point3D tgt = a.getPt0().plus(d);
        if (type == TargetType.groundTillEnemy) {
            // all empty cells in direction up to enemy
            while (m.inBounds(tgt.x, tgt.y)) {
                MapObject obj = m.getTopmost(tgt);
                if (groundOrEnemy.apply(a, obj)) {
                    points.add(tgt);
                }
                if (obj instanceof Unit) break;
                tgt = tgt.plus(d);
            }
        } else if (type == TargetType.enemy) {
            // one cell with enemy placed on it
            MapObject obj = m.getTopmost(tgt);
            if (obj instanceof Unit && ((Unit) obj).team != a.team) {
                points.add(tgt);
            }
        } else {
            // single cell
            MapObject obj = m.getTopmost(tgt);
            if (groundOrEnemy.apply(a, obj)) {
                points.add(tgt);
            }
        }
        return points;
    }

    public enum TargetType {
        ground,
        groundTillEnemy,
        enemy,
        any
    }
}
