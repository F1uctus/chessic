package com.f1uctus.chessic.setting;

public class MapDefiner {
    private int xSize;
    private int ySize;
    private int zSize;
    private String formula;

    MapDefiner() {
    }

    public int getXSize() {
        return xSize;
    }

    public void setXSize(int xSize) {
        this.xSize = xSize;
    }

    public int getYSize() {
        return ySize;
    }

    public void setYSize(int ySize) {
        this.ySize = ySize;
    }

    public int getZSize() {
        return zSize;
    }

    public void setZSize(int zSize) {
        this.zSize = zSize;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
}
