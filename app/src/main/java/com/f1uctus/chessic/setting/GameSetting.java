package com.f1uctus.chessic.setting;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import com.crown.common.ObjectsMap;
import com.crown.maps.Point3D;
import com.f1uctus.chessic.GameState;
import com.f1uctus.chessic.maps.GameMap;
import com.f1uctus.chessic.maps.GraphicalMapIcon;
import com.f1uctus.chessic.objects.Ground;
import com.f1uctus.chessic.tools.FileTools;
import com.f1uctus.chessic.tools.ImageTools;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import bsh.EvalError;
import bsh.Interpreter;

import static com.f1uctus.chessic.tools.SystemTools.showMessage;
import static com.f1uctus.chessic.tools.SystemTools.showToast;

public class GameSetting {
    public String name;
    public MapDefiner map;
    public List<Team> teams = new ArrayList<>();
    public List<UnitDefiner> units = new ArrayList<>();
    public List<Arrange> arrangement = new ArrayList<>();

    public transient File path;

    public GameSetting() {
    }

    public static GameSetting loadFrom(Context context, File settingDir) {
        File settingYml = new File(settingDir, "setting.yml");
        if (!settingYml.exists()) {
            showMessage(context, "Unable to load setting from " + settingDir);
            return null;
        }
        try (FileInputStream fis = new FileInputStream(settingYml)) {
            String yamlSource = FileTools.readToString(fis);
            Yaml y = new Yaml(new SettingRepresenter());
            GameSetting gs = y.load(yamlSource);
            gs.path = settingDir;
            return gs;
        } catch (Exception e) {
            showMessage(context, settingDir.getName(), e.toString());
            return null;
        }
    }

    public GameState makeState(Context context) {
        GameState gs = new GameState(
            context,
            this,
            new GameMap(map.getXSize(), map.getYSize(), map.getZSize())
        );

        ObjectsMap<GraphicalMapIcon> defaultIconSet = gs.iconThemes.get("default");
        if (defaultIconSet == null) {
            defaultIconSet = new ObjectsMap<>();
            gs.iconThemes.put("default", defaultIconSet);
        }

        Interpreter i = new Interpreter();
        try {
            i.eval(map.getFormula());
            boolean doZGeneration = Arrays.equals(
                i.getNameSpace().getMethods()[0].getParameterNames(),
                new String[] { "x", "y", "z" }
            );
            int zBound = doZGeneration ? map.getZSize() : 1;
            for (int x = 0; x < map.getXSize(); x++) {
                for (int y = 0; y < map.getYSize(); y++) {
                    for (int z = 0; z < zBound; z++) {
                        String keyName = "bg_" + x + "" + y + "" + z;
                        GraphicalMapIcon icon = defaultIconSet.get(keyName);
                        if (icon == null) {
                            i.set("x", x);
                            i.set("y", y);
                            i.set("z", z);
                            Object result = doZGeneration
                                ? i.eval("f(" + x + "," + y + "," + z + ")")
                                : i.eval("f(" + x + "," + y + ")");
                            if (result instanceof String) {
                                String tile = (String) result;
                                if (tile.startsWith("#")
                                    && tile.length() == 7 || tile.length() == 9) {
                                    int clr = Color.parseColor(tile);
                                    Bitmap bmp = ImageTools.pixelBitmap(clr);
                                    icon = new GraphicalMapIcon(keyName, bmp);
                                    defaultIconSet.add(icon);

                                }
                            } else {
                                // TODO bitmap
                            }
                        }
                        new Ground(
                            keyName,
                            gs.getGlobalMap(),
                            icon,
                            new Point3D(x, y, z)
                        );
                    }
                }
            }
        } catch (EvalError evalError) {
            showMessage(context, "Map definition error", evalError.toString());
            return null;
        }

        for (Arrange a : arrangement) {
            Optional<UnitDefiner> definer = units.stream()
                .filter(u -> u.getName().equals(a.getUnit()))
                .findFirst();
            if (definer.isPresent()) {
                Optional<Team> team = teams.stream()
                    .filter(t -> t.getName().equals(a.getTeam()))
                    .findFirst();
                if (team.isPresent()) {
                    gs.addUnit(
                        definer.get(),
                        team.get(),
                        new Point3D(a.getX() - 1, a.getY() - 1, 1)
                    );
                } else {
                    showToast(
                        context,
                        "Failed to build game: Team '" + a.getTeam() + "' doesn't exists."
                    );
                    return null;
                }
            } else {
                showToast(
                    context,
                    "Failed to build game: Unit '" + a.getUnit() + "' doesn't exists"
                );
                return null;
            }
        }
        return gs;
    }
}
