package com.f1uctus.chessic.setting;

import com.crown.maps.Direction;

public class Team {
    private String name;
    private String theme;
    private String color;
    private Direction startDirection;

    Team() {
    }

    public Team(String name, String theme, String color, Direction startDirection) {
        this.name = name;
        this.theme = theme;
        this.color = color;
        this.startDirection = startDirection;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Direction getStartDirection() {
        return startDirection;
    }
}
