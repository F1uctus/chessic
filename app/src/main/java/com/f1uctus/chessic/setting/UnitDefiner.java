package com.f1uctus.chessic.setting;

public class UnitDefiner {
    private String name;
    private Movement[] allowedMovements;

    UnitDefiner() {
    }

    public UnitDefiner(String name, Movement[] allowedMovements) {
        this.name = name;
        this.allowedMovements = allowedMovements;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Movement[] getAllowedMovements() {
        return allowedMovements;
    }

    public void setAllowedMovements(Movement[] allowedMovements) {
        this.allowedMovements = allowedMovements;
    }
}