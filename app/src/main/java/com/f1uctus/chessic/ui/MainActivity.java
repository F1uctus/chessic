package com.f1uctus.chessic.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.crown.i18n.I18n;
import com.f1uctus.chessic.R;
import com.f1uctus.chessic.setting.GameSetting;
import com.f1uctus.chessic.tools.FileTools;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static com.f1uctus.chessic.tools.SystemTools.showToast;

public class MainActivity extends AppCompatActivity {
    public static final HashMap<String, ResourceBundle> bundles = new HashMap<>();
    public static final String locale = Locale.getDefault().getLanguage();
    public static final HashMap<String, GameSetting> settings = new HashMap<>();
    public static File dataDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        dataDir = new File(getApplicationInfo().dataDir);
        if (!dataDir.exists()) {
            dataDir.mkdirs();
        }
        Log.d("files", FileTools.walk(dataDir).stream().map(File::toString).collect(Collectors.joining("\n")));
        Arrays.stream(dataDir.listFiles()).forEach(f -> {
            File settingFile = new File(f.getAbsoluteFile(), "setting.yml");
            if (f.isDirectory() && settingFile.exists()) {
                GameSetting gs = GameSetting.loadFrom(this, f);
                if (gs != null) {
                    settings.put(gs.name, gs);
                }
            }
        });

        Log.d("I18n", "Initializing...");
        // bundles.put("ru", new Resources.Russian());
        // bundles.put("en", new Resources.English());
        I18n.init(bundles);
        Log.d("I18n", "OK");
    }

    public void onPlayClick(View view) {
        if (settings.size() == 0) {
            showToast(this, "You have no game settings available!");
            return;
        }
        startActivity(new Intent(this, GameActivity.class));
    }

    public void onEditorClick(View view) {
        startActivity(new Intent(this, SettingManagerActivity.class));
    }

    public void onSettingsClick(View view) {
        startActivity(new Intent(this, OptionsActivity.class));
    }
}