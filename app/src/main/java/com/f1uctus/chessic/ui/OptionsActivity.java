package com.f1uctus.chessic.ui;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.ListPreference;
import androidx.preference.PreferenceFragmentCompat;

import com.f1uctus.chessic.R;

import static com.f1uctus.chessic.ui.MainActivity.settings;

public class OptionsActivity extends AppCompatActivity {
    public static final String GAME_SETTING = "game_setting";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return false;
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            ListPreference settingLp = findPreference(GAME_SETTING);
            CharSequence[] values = new CharSequence[settings.size()];
            if (settings.size() > 0) {
                int idx = 0;
                for (String s : settings.keySet()) {
                    values[idx++] = s;
                }
                settingLp.setDefaultValue(values[0]);
            }
            settingLp.setEntries(values);
            settingLp.setEntryValues(values);
        }
    }
}