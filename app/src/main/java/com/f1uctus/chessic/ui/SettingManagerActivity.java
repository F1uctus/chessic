package com.f1uctus.chessic.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.f1uctus.chessic.R;
import com.f1uctus.chessic.setting.GameSetting;
import com.f1uctus.chessic.tools.FileTools;

import java.io.File;
import java.io.IOException;

import static com.f1uctus.chessic.tools.SystemTools.showToast;
import static com.f1uctus.chessic.ui.MainActivity.dataDir;
import static com.f1uctus.chessic.ui.MainActivity.settings;

public class SettingManagerActivity extends AppCompatActivity {
    private static String selectedListItemTitle;
    private final int READ_ZIP_REQUEST_CODE = 1337;
    private final int WRITE_ZIP_REQUEST_CODE = 1338;
    ArrayAdapter<String> settingListItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_menu);

        ListView listView = findViewById(R.id.settings_lw);
        settingListItems = new ArrayAdapter<>(this, R.layout.list_item_game_setting_simple);
        settingListItems.addAll(settings.keySet());
        listView.setAdapter(settingListItems);
        listView.setOnItemClickListener((parent, view, position, id) -> showListItemMenu(view));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return false;
    }

    public void showListItemMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.list_item_game_setting_popup_menu);
        popupMenu.setOnMenuItemClickListener(item -> {
            int itemId = item.getItemId();
            selectedListItemTitle = ((TextView) v).getText().toString();
            if (itemId == R.id.menu_set_as_default) {
                showToast(this, "deprecated");
                return true;
            } else if (itemId == R.id.menu_edit) {
                showToast(this, "deprecated");
                return true;
            } else if (itemId == R.id.menu_export) {
                if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this,
                        new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                        WRITE_ZIP_REQUEST_CODE
                    );
                }
                Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                intent.setType("application/zip");
                startActivityForResult(intent, WRITE_ZIP_REQUEST_CODE);
                return true;
            } else if (itemId == R.id.menu_remove) {
                new AlertDialog.Builder(this)
                    .setMessage("Do you really want to remove '" + selectedListItemTitle + "' setting?")
                    .setPositiveButton(android.R.string.yes, (dialog, buttonId) -> {
                        FileTools.deleteRecursive(settings.get(selectedListItemTitle).path);
                        settings.remove(selectedListItemTitle);
                        settingListItems.remove(selectedListItemTitle);
                    })
                    .setNegativeButton(android.R.string.no, null).show();
                return true;
            }
            return false;
        });
        popupMenu.show();
    }

    public void onImportClick(View view) {
        if (ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                READ_ZIP_REQUEST_CODE
            );
        }
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/zip");
        startActivityForResult(intent, READ_ZIP_REQUEST_CODE);
    }


    public void onNewSettingClick(View view) {
        startActivity(new Intent(this, SettingEditorActivity.class));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK && intent != null && intent.getData() != null) {
            try {
                if (requestCode == READ_ZIP_REQUEST_CODE) {
                    Uri zipPath = intent.getData();

                    File tempOutDir = FileTools.unzip(
                        getContentResolver().openInputStream(zipPath),
                        new File(dataDir, "temp")
                    );
                    GameSetting gs = GameSetting.loadFrom(this, tempOutDir);
                    if (settings.containsKey(gs.name)) {
                        showToast(this, "A setting with specified name already exists.");
                        tempOutDir.delete();
                        return;
                    }
                    gs.path = FileTools.popParentDir(tempOutDir);
                    settings.put(gs.name, gs);
                    settingListItems.add(gs.name);
                    showToast(this, "Setting was imported");
                } else if (requestCode == WRITE_ZIP_REQUEST_CODE) {
                    Uri zipPath = intent.getData();
                    GameSetting gs = settings.get(selectedListItemTitle);

                    FileTools.zip(gs.path, getContentResolver().openOutputStream(zipPath));
                    showToast(this, "Setting was exported");
                }
            } catch (Exception e) {
                e.printStackTrace();
                showToast(this, e.getMessage());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(
        int requestCode,
        @NonNull String[] permissions,
        @NonNull int[] grantResults
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            showToast(this, "Storage permission denied");
        }
    }
}