package com.f1uctus.chessic.ui.setting_editor.units;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class UnitsViewModel extends ViewModel {

    private final MutableLiveData<String> mText;

    public UnitsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}