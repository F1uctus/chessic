package com.f1uctus.chessic.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.crown.maps.Map;
import com.crown.maps.MapObject;
import com.crown.maps.Point3D;
import com.crown.time.Timeline;
import com.f1uctus.chessic.creatures.Unit;
import com.f1uctus.chessic.setting.Movement;
import com.f1uctus.chessic.tools.ImageTools;

import static com.f1uctus.chessic.tools.SystemTools.showToast;

public class MapView extends View {
    static final Paint moveEnemyPaint = new Paint();
    static final Paint movePaint = new Paint();
    static final Paint tilePaint = new Paint();
    Map map;
    Unit unit;

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);

        moveEnemyPaint.setColor(Color.argb(100, 180, 10, 10));
        movePaint.setColor(Color.argb(100, 250, 190, 0));
        tilePaint.setColor(Color.WHITE);
    }

    protected void onDraw(Canvas canvas) {
        map = Timeline.main.getGameState().getGlobalMap();
        int tileSide = getTileSide();
        for (int areaZ = 0; areaZ < map.zSize; areaZ++) {
            for (int areaY = 0; areaY < map.ySize; areaY++) {
                for (int areaX = 0; areaX < map.xSize; areaX++) {
                    MapObject o = map.get(areaX, areaY, areaZ);
                    if (o != null) {
                        canvas.drawBitmap(
                            ImageTools.resizeTile((Bitmap) o.getMapIcon().get(), tileSide, tileSide),
                            areaX * tileSide,
                            areaY * tileSide,
                            tilePaint
                        );
                    }
                }
            }
        }
        // draw available unit's movements as dots
        if (unit != null) {
            for (Movement move : unit.movements) {
                for (Point3D pt : move.getPoints(unit)) {
                    Paint p = (map.getTopmost(pt) instanceof Unit) ? moveEnemyPaint : movePaint;
                    canvas.drawCircle(
                        pt.x * tileSide + tileSide / 2f,
                        pt.y * tileSide + tileSide / 2f,
                        tileSide / 5f,
                        p
                    );
                }
            }
        }
    }

    public int getTileSide() {
        return getWidth() / map.xSize;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        if ((action & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_DOWN) {
            float x = event.getX() - getLeft();
            float y = event.getY() - getTop() + getTileSide() / 2f;

            int xPos = (int) (map.xSize * (x / getWidth()));
            int yPos = (int) (map.ySize * (y / getHeight()));
            Log.d("MapView", "Touch " + xPos + ", " + yPos);

            Point3D point = new Point3D(xPos, yPos, map.zSize);
            MapObject o = map.getTopmost(point);

            if (unit != null) {
                Movement move = unit.tryGetMove(xPos, yPos);
                if (move != null && move.isAllowed(unit, o)) {
                    if (o instanceof Unit) {
                        map.remove(o);
                    }
                    unit.moveBy(point.x - unit.getPt0().x, point.y - unit.getPt0().y);
                }
                unit = null;
            } else if (o instanceof Unit) {
                unit = (Unit) o;
                // TODO deselect if no movements available
            }
            invalidate();
            return true;
        }
        return false;
    }
}