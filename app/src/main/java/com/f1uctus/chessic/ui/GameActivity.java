package com.f1uctus.chessic.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.crown.creatures.Organism;
import com.crown.time.Timeline;
import com.crown.time.VirtualClock;
import com.f1uctus.chessic.GameState;
import com.f1uctus.chessic.R;
import com.f1uctus.chessic.setting.GameSetting;

import java.time.Duration;
import java.time.Instant;

import static com.f1uctus.chessic.ui.MainActivity.locale;
import static com.f1uctus.chessic.ui.MainActivity.settings;
import static com.f1uctus.chessic.ui.OptionsActivity.GAME_SETTING;

public class GameActivity extends AppCompatActivity {
    GameState state;
    Instant gameStartTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        GameSetting gs = settings.get(getPrefs().getString(GAME_SETTING, ""));
        if (gs == null) {
            gs = settings.values().stream().findFirst().get();
        }
        state = gs.makeState(this);
        gameStartTime = Instant.now();

        Timeline.init(
            new VirtualClock(1000, () -> {
                runOnUiThread(() -> {
                    TextView time = findViewById(R.id.game_time);
                    Duration d = Duration.between(gameStartTime, Timeline.getClock().now());
                    time.setText(formatDuration(d));
                });
            }).startAt(Instant.now()),
            state
        );

        for (Organism o : state.players) {
            o.setTimeline(Timeline.main);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Timeline.main = null;
    }

    public static String formatDuration(Duration duration) {
        long seconds = duration.getSeconds();
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
            locale,
            "%d:%02d:%02d",
            absSeconds / 3600,
            (absSeconds % 3600) / 60,
            absSeconds % 60
        );
        return seconds < 0 ? "-" + positive : positive;
    }

    public SharedPreferences getPrefs() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }
}