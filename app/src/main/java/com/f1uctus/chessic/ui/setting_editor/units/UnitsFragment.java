package com.f1uctus.chessic.ui.setting_editor.units;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.f1uctus.chessic.R;

public class UnitsFragment extends Fragment {

    private UnitsViewModel unitsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        unitsViewModel = new ViewModelProvider(this).get(UnitsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_editor_units, container, false);
        final TextView textView = root.findViewById(R.id.text_units);
        unitsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}