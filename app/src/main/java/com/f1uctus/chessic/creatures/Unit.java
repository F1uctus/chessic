package com.f1uctus.chessic.creatures;

import com.crown.creatures.Organism;
import com.crown.i18n.I18n;
import com.crown.i18n.ITemplate;
import com.crown.maps.Direction;
import com.crown.maps.Map;
import com.crown.maps.MapIcon;
import com.crown.maps.Point3D;
import com.crown.time.Timeline;
import com.f1uctus.chessic.GameState;
import com.f1uctus.chessic.maps.GraphicalMapIcon;
import com.f1uctus.chessic.setting.Movement;
import com.f1uctus.chessic.setting.Team;
import com.f1uctus.chessic.setting.UnitDefiner;

import java.util.Arrays;
import java.util.Optional;

public class Unit extends Organism {
    public final Team team;
    public final Movement[] movements;

    public Unit(
        String name,
        Map map,
        GraphicalMapIcon icon,
        Point3D pt,
        Team team,
        UnitDefiner definer
    ) {
        super(
            name,
            map,
            icon,
            pt,
            Integer.MAX_VALUE,
            1,
            1,
            1
        );
        this.team = team;
        movements = definer.getAllowedMovements().clone();
        Point3D startDirPt = team.getStartDirection().point;
        for (int i = 0; i < movements.length; i++) {
            Movement mv = movements[i];
            int xMul = 1;
            if (startDirPt.x != 0) xMul *= startDirPt.x;
            int yMul = 1;
            if (startDirPt.y != 0) yMul *= startDirPt.y;
            if (mv.getDirection() == null
                || mv.getDirection() == Direction.none) {
                movements[i] = new Movement(
                    mv.getX() * xMul,
                    mv.getY() * yMul,
                    mv.getType()
                );
            } else {
                movements[i] = new Movement(
                    Direction.fromPoint(mv.getDirection().point.mul(
                        xMul, yMul, 1
                    )),
                    mv.getType()
                );
            }

        }
    }

    public Movement tryGetMove(int x, int y) {
        for (Movement m : movements) {
            if (m.contains(this, x, y)) {
                return m;
            }
        }
        return null;
    }

    @Override
    public MapIcon<?> getMapIcon() {
        return ((GameState) Timeline.main.getGameState()).iconThemes.get(team.getTheme()).get(getMapIconId());
    }

    @Override
    public ITemplate getStats() {
        return I18n.empty;
    }
}
