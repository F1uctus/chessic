package com.f1uctus.chessic.tools;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class FileTools {
    public static File popParentDir(File file) {
        File result = new File(file.getParentFile().getParentFile(), file.getName());
        file.renameTo(result);
        return result;
    }

    public static List<File> walk(File root) {
        List<File> out = new ArrayList<>();
        File[] list = root.listFiles();
        for (File f : list) {
            if (f.isDirectory()) {
                out.add(f.getAbsoluteFile());
                out.addAll(walk(f));
            } else {
                out.add(f.getAbsoluteFile());
            }
        }
        return out;
    }

    public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        fileOrDirectory.delete();
    }

    public static String nameWithoutExtension(File f) {
        return f.getName().replaceFirst("[.][^.]+$", "");
    }

    public static String readToString(InputStream is) throws Exception {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }
        return sb.toString();
    }

    public static File unzip(File input, File targetDir) throws IOException {
        return unzip(new FileInputStream(input), targetDir);
    }

    public static File unzip(InputStream input, File targetDir) throws IOException {
        File upperFile = null;
        try (ZipInputStream zis = new ZipInputStream(
            new BufferedInputStream(input))) {
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                File targetPath = new File(targetDir, ze.getName());
                if (!targetPath.isDirectory() && (upperFile == null || targetPath.getPath().length() < upperFile.getPath().length())) {
                    upperFile = targetPath;
                }
                File dir = ze.isDirectory() ? targetPath : targetPath.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " + dir.getAbsolutePath());
                if (ze.isDirectory())
                    continue;
                try (FileOutputStream fout = new FileOutputStream(targetPath)) {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                }
            }
        }
        return upperFile.getParentFile();
    }

    public static void zip(File dir, File output) throws IOException {
        zip(dir, new FileOutputStream(output));
    }

    public static void zip(File dir, OutputStream output) throws IOException {
        try (ZipOutputStream out = new ZipOutputStream(output)) {
            zipSubFolder(out, dir, dir.getParentFile().getPath().length());
        }
    }

    private static void zipSubFolder(ZipOutputStream out, File folder, int basePathLength) throws IOException {
        final int BUFFER = 2048;
        File[] fileList = folder.listFiles();
        BufferedInputStream origin;
        for (File file : fileList) {
            if (file.isDirectory()) {
                zipSubFolder(out, file, basePathLength);
            } else {
                byte[] data = new byte[BUFFER];

                String unmodifiedFilePath = file.getPath();
                String relativePath = unmodifiedFilePath.substring(basePathLength + 1);

                FileInputStream fi = new FileInputStream(unmodifiedFilePath);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(relativePath);
                entry.setTime(file.lastModified());
                out.putNextEntry(entry);

                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
                out.closeEntry();
            }
        }
    }
}
