package com.f1uctus.chessic.tools;

import android.content.Context;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

public class SystemTools {
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showMessage(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
            .setPositiveButton("OK", (dialog, id) -> {
                dialog.cancel();
            });
        builder.create().show();
    }

    public static void showMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
            .setMessage(message)
            .setPositiveButton("OK", (dialog, id) -> {
                dialog.cancel();
            });
        builder.create().show();
    }
}
