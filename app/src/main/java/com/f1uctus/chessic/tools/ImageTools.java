package com.f1uctus.chessic.tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;

import com.crown.common.utils.Random;

import java.util.HashMap;
import java.util.Objects;

public class ImageTools {
    public static Bitmap pixelBitmap(int pixelColor) {
        Bitmap bmp = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        canvas.drawColor(pixelColor);
        return bmp;
    }

    public static Bitmap loadBitmap(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(path, options);
    }

    public static Bitmap squareBitmap(Bitmap bitmap) {
        return squareBitmap(bitmap, 0);
    }

    public static Bitmap squareBitmap(Bitmap bitmap, int padding) {
        int dim = Math.max(bitmap.getWidth(), bitmap.getHeight()) + padding * 2;
        Bitmap bmp = Bitmap.createBitmap(dim, dim, Bitmap.Config.ARGB_8888);
        int leftOffset = Math.abs(bmp.getWidth() - bitmap.getWidth()) / 2;
        int topOffset = Math.abs(bmp.getHeight() - bitmap.getHeight()) / 2;
        Canvas canvas = new Canvas(bmp);
        canvas.drawBitmap(bitmap, leftOffset, topOffset, null);
        return bmp;
    }

    public static void addTint(Bitmap bitmap, int tintColor) {
        Canvas canvas = new Canvas(bitmap);
        Paint p = new Paint();
        p.setColorFilter(new PorterDuffColorFilter(tintColor, PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, 0, 0, p);
    }

    private static final HashMap<ResizeArgs, Bitmap> resizeTileCache = new HashMap<>();

    public static Bitmap resizeTile(Bitmap tile, int w, int h) {
        ResizeArgs args = new ResizeArgs(tile, w, h);
        Bitmap cached = resizeTileCache.getOrDefault(args, null);
        if (cached != null) {
            return cached;
        }
        Bitmap generated = resize(tile, w, h);
        resizeTileCache.put(args, generated);
        return generated;
    }

    private static Bitmap resize(Bitmap img, int width, int height) {
        return Bitmap.createScaledBitmap(
            img,
            width,
            height,
            true
        );
    }

    public static int getRandomColor() {
        return Color.rgb(
            Random.getInt(50, 200),
            Random.getInt(50, 200),
            Random.getInt(50, 200)
        );
    }

    private static class ResizeArgs {
        Bitmap image;
        int w;
        int h;

        public ResizeArgs(Bitmap image, int w, int h) {
            this.image = image;
            this.w = w;
            this.h = h;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ResizeArgs that = (ResizeArgs) o;
            return w == that.w && h == that.h && image.equals(that.image);
        }

        @Override
        public int hashCode() {
            return Objects.hash(image, w, h);
        }
    }
}
