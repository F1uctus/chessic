package com.f1uctus.chessic.tools;

import java.util.Collection;

public class MathTools {
    public static int maxIgnoreSign(int a, int b) {
        return Math.abs(a) > Math.abs(b) ? a : Math.max(a, b);
    }

    public static int indexOfMaxIgnoreSign(Collection<Integer> values) {
        int maxValue = 0;
        int maxIdx = 0;
        int idx = 0;
        for (int v : values) {
            v = Math.abs(v);
            if (v > maxValue) {
                maxValue = v;
                maxIdx = idx;
            }
            idx++;
        }
        return maxIdx;
    }
}
