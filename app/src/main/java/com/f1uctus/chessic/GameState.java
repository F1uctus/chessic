package com.f1uctus.chessic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;

import com.crown.BaseGameState;
import com.crown.common.ObjectsMap;
import com.crown.maps.Map;
import com.crown.maps.Point3D;
import com.f1uctus.chessic.creatures.Unit;
import com.f1uctus.chessic.maps.GraphicalMapIcon;
import com.f1uctus.chessic.setting.GameSetting;
import com.f1uctus.chessic.setting.Team;
import com.f1uctus.chessic.setting.UnitDefiner;
import com.f1uctus.chessic.tools.ImageTools;

import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Contains game state for current running session.
 */
public class GameState extends BaseGameState {
    public final Context context;
    public final GameSetting setting;
    public final HashMap<String, ObjectsMap<GraphicalMapIcon>> iconThemes = new HashMap<>();

    public GameState(
        Context context,
        GameSetting setting,
        Map map
    ) {
        super(map);
        this.context = context;
        this.setting = setting;
    }

    public void addUnit(UnitDefiner definer, Team team, Point3D pt) {
        String name = definer.getName();
        ObjectsMap<GraphicalMapIcon> theme = iconThemes.get(team.getTheme());

        if (theme == null) {
            theme = new ObjectsMap<>();
            iconThemes.put(team.getTheme(), theme);
        }
        String iconName = team.getName() + "_" + name;
        if (theme.get(iconName) == null) {
            Bitmap bmp = ImageTools.loadBitmap(Paths.get(
                setting.path.getPath(),
                "themes",
                team.getTheme(),
                name + ".png"
            ).toString());
            ImageTools.addTint(bmp, Color.parseColor(team.getColor()));
            bmp = ImageTools.squareBitmap(bmp, 15);

            theme.add(new GraphicalMapIcon(iconName, bmp));
        }

        int uniqId = 1;
        while (players.get(name + uniqId) != null) uniqId++;

        players.add(new Unit(
            name + uniqId,
            getGlobalMap(),
            theme.get(iconName),
            pt,
            team,
            definer
        ));
    }
}